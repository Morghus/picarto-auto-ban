# Picarto Auto Ban Userscript

## Usage

Install [Tampermonkey](https://www.tampermonkey.net/) or [Violentmonkey](https://violentmonkey.github.io/).

-   Tampermonkey: Go to "Dasboard", then "Utilities", copy and paste this URL into the "URL" field and click "Load":

    `https://bitbucket.org/Morghus/picarto-auto-ban/downloads/userscript.js`

## Development

-   `npm i`
-   `npm run debug`

Output is in `dist/userscript.js`.
