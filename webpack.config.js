const path = require('path'),
    webpack = require('webpack'),
    stripIndent = require('common-tags').stripIndent,
    env = process.env.NODE_ENV || 'development',
    UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
    execa = require('execa');

// const gitHash = execa.sync('git', ['rev-parse', '--short', 'HEAD']).stdout;
const gitNumCommits = Number(execa.sync('git', ['rev-list', 'HEAD', '--count']).stdout);
const gitBranch = execa.sync('git', ['rev-parse', '--abbrev-ref', 'HEAD']).stdout;
const gitDirty = execa.sync('git', ['status', '-s', '-uall']).stdout.length > 0;

const version = `${gitNumCommits + (gitDirty ? 1 : 0)}${
    gitBranch === 'master' ? '' : `@${gitBranch}`
}${gitDirty ? '#dev' : ''}`;

module.exports = {
    entry: './src/index.ts',
    devtool: env === 'production' ? 'none' : 'eval-source-map',
    mode: env,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'userscript.js',
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: /==\/?UserScript==|^ @/,
                    },
                },
            }),
        ],
    },
    plugins: [
        new webpack.BannerPlugin({
            raw: true,
            banner: stripIndent`
                // ==UserScript==
                // @name        Picarto Auto Ban
                // @namespace   https://bitbucket.org/Morghus/picarto-auto-ban
                // @version     ${version}
                // @author      Morghus
                // @description A script that bans offensive users from Picarto
                // @downloadURL https://bitbucket.org/Morghus/picarto-auto-ban/downloads/userscript.js
                // @updateURL   https://bitbucket.org/Morghus/picarto-auto-ban/downloads/userscript.js
                // @include     https://picarto.tv/*
                // ==/UserScript==`,
        }),
    ],
};
