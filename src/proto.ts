declare const proto: {
    NewMessage: new (messages: string[]) => NewMessage;
    RemoveMessage: new (messageIds: number[]) => RemoveMessage;
    Control: { MessageType: { [key: string]: number } };
};
export interface Message { }
export interface NewMessage extends Message { }
export interface RemoveMessage extends Message { }

export default proto;
