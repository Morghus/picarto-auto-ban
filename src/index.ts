import proto from './proto';
import socket from './socket';

const spamMessageThreshold = 2;
const spamMessageLengthThreshold = 26;
const scoreThreshold = 10;

const MessageType = Object.assign(
    {},
    ...Object.entries(proto.Control.MessageType).map(([a, b]) => ({ [b]: a }))
);

class Spammer {
    name: string;
    score: number = 0;
    messages: number = 0;
    constructor(name: string) {
        this.name = name;
    }

    hasValues() {
        return this.score > 0 || this.messages > 0;
    }
}

const spammers: { [key: string]: Spammer } = {};

const scoring = {
    1: [
        /retard/g,
        /rotten/g,
        /human trash/g,
        /pussy/g,
        /falklands/g,
        /sexualizing/g,
        /rape/g,
        /alzheimer/g,
        /[\u0300-\uffff]/g,
    ],
    9: [
        /literal retard/g,
        /swarthy, oily nerd/g,
        /adam and jimmy/g,
        /literal subhuman/g,
        /degenerate sentiment/g,
    ],
    10: [/subhuman/g, /faggot/g],
};

const removeKeywords = [/covid/g, /virus/g, /corona/g, /plague/g, /quarantine/g, /isolat(e|ion)/g];

function withSpammer(name: string, cb: (spammer: Spammer) => void) {
    if (!spammers[name]) {
        spammers[name] = new Spammer(name);
    }
    cb(spammers[name]);
}

function addScore(name: string, score: number) {
    withSpammer(name, spammer => (spammer.score += score));
}

function addMessage(name: string) {
    withSpammer(name, spammer => spammer.messages++);
}

function removeMessage(id: number) {
    socket.Emit('RemoveMessage', new proto.RemoveMessage([id]));
}

function onMessages(username: string, message: string, id: number) {
    if (message.length >= spamMessageLengthThreshold) {
        addMessage(username);
    }
    for (let [score, words] of Object.entries(scoring)) {
        for (const word of words) {
            let result;
            while ((result = word.exec(message.toLowerCase()))) {
                console.log(username, 'said', result[0], 'adding', score, 'score');
                addScore(username, Number(score));
            }
        }
    }
    maybeBanSpammer(username);
}

function maybeBanSpammer(username: string) {
    if (spammers[username]) {
        console.log(username, spammers[username]);
        if (
            spammers[username].score >= scoreThreshold ||
            spammers[username].messages >= spamMessageThreshold
        ) {
            console.log(username, 'exceeded score, banning...');
            ['/cu', '/sb'].forEach(command =>
                socket.Emit('NewMessage', new proto.NewMessage([command + ' ' + username]))
            );
            delete spammers[username];
        }
    }
}

function falloffAndPrune(prop: 'score' | 'messages') {
    for (const [name, spammer] of Object.entries(spammers)) {
        if (spammer[prop] > 0) {
            spammer[prop]--;
        }
        if (!spammer.hasValues()) {
            console.log('Spammer', name, 'has reached 0');
            delete spammers[name];
        }
    }
}

let readingHistory = true;
socket.On('Connect', () => (readingHistory = true));
socket.On('Reconnect', () => (readingHistory = true));
socket.On('Control', obj => {
    if (MessageType[obj.messageType] === 'END_HISTORY') {
        readingHistory = false;
        console.log('Finished reading history');
    }
});
socket.On('ChatMessage', obj => {
    if (readingHistory) {
        return;
    }
    if (!obj.streamer && !obj.moderator && !obj.ptvAdmin) {
        for (let removeKeyword of removeKeywords) {
            if (obj.message.toLowerCase().match(removeKeyword)) {
                console.debug('Message ', obj, ' matched keyword ', removeKeyword, ', removing message!');
                removeMessage(obj.id);
                return;
            }
        }
    }
    /*if (
        !obj.subscriber &&
        !obj.moderator &&
        !obj.streamer &&
        !obj.ptvAdmin &&
        !obj.premium
    ) {
        onMessages(obj.displayName, obj.message, obj.id);
    }*/
});

setInterval(function () {
    falloffAndPrune('score');
}, 15000);

setInterval(function () {
    falloffAndPrune('messages');
}, 5000);
