import { Message } from './proto';

declare const socket: SocketInterface;

export interface SocketInterface {
    Emit(event: string, obj: Message): void;
    On(event: 'ChatMessage', callback: (obj: ChatMessage) => void): void;
    On(event: 'Control', callback: (obj: Control) => void): void;
    On(event: 'Reconnect', callback: () => void): void;
    On(event: 'Connect', callback: () => void): void;
}

export interface ChatMessage {
    id: number;
    userId: number;
    displayName: string;
    message: string;
    registered: boolean;
    moderator: boolean;
    streamer: boolean;
    ptvAdmin: boolean;
    basic: boolean;
    premium: boolean;
    subscriber: boolean;
    color: string;
    timeStamp: number;
    isMeMessage: boolean;
    bot: boolean;
}
export interface Control {
    messageType: number;
    dataBool: boolean;
    channel: number;
    channelName: string;
}

export default socket;
